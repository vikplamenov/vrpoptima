
require(ggplot2)

#' Graphical display of the routes created with the genetic program.
#'
#' @param solution The object returned from VehicleRouting().
#' @param routes_list A list object containing the routes for each agent obtained by calling RoutesDataPrep().
#' @param agent_locations    The positions of the agents (vehicles).
#' @param orientation Controls the orientation of the figures. Options include vertical and horizontal orientation.
#'
#' @return A figure displaying the vehicle positions (depots) as black squares and the quasi-optimal routes. the vignettes for examples how to run the optimizer and visualize the routes.
#' @export
#' @import ggplot2
#' @examples
PlotToursCombined <- function(solution = NULL,
                              routes_list = NULL,
                              agent_locations = NULL,
                              orientation = 'vertical'){

  if(is.null(solution)){
    stop('Please provide the solution object obtained by calling VehicleRouting()')
  }
  if(is.null(routes_list)){
    stop('Please provide the preprocessed routes list obtained by calling RoutesDataPrep()')
  }


  paths_df = dplyr::bind_rows(routes_list, .id = 'vehicle')

  fig1 <-
    ggplot(paths_df)+
    geom_point(aes(lon, lat), color = 'black', shape = 15, size = 5, data = agent_locations)+
    geom_path(aes(lon, lat, col = vehicle))+
    geom_point(aes(lon, lat, col = vehicle), size = 2.5)+
    theme_gray(base_size = 16)+
    theme(axis.ticks = element_line(size = 2, color = 'black'),
          legend.position = 'none',
          axis.line = element_line(colour = 'black', size = 1.5))+
    ggtitle('Proposed Routes')+
    xlab('Longitude')+
    ylab('Latitude')

  # Optimization history
  historical_performance <- data.frame(Generation = 1:length(as.numeric(solution$dist_history)),
                                       Fitness = as.numeric(solution$dist_history))


  fig2 <-
    ggplot(historical_performance, aes(x = Generation, y = Fitness))+
    geom_area(fill = 'darkblue', alpha = 0.85)+
    geom_point()+
    theme_gray(base_size = 16)+
    theme(axis.line = element_line(colour = 'black', size = 1.5))+
    ggtitle(label = 'Model Performance', subtitle = 'Optimization History')

  if(orientation == 'vertical'){
      ncol = 1
  } else if(orientation == 'horizontal'){
      ncol = 2
  } else{
      ncol = 2
  }

  gridExtra::grid.arrange(fig1, fig2, ncol = ncol)

}



#' Graphical display of the routes created with the genetic program.
#'
#' @param solution The object returned from VehicleRouting().
#' @param routes_list A list object containing the routes for each agent obtained by calling RoutesDataPrep().
#' @return A figure displaying the vehicle positions (depots) as black squares and the quasi-optimal routes.
#' @export
#' @examples
PlotToursIndividual <- function(solution = NULL, routes_list = NULL){


  paths_df = dplyr::bind_rows(routes_list, .id = 'vehicle')

  routes <- solution$routes
  paths_df$vehicle <- paste0('Vehicle ', paths_df$vehicle)

  ggplot(paths_df)+
    geom_path(aes(lon, lat))+
    geom_point(aes(lon, lat), col = 'blue', size = 2.5)+
    facet_wrap(~vehicle, ncol = 5, scales = 'free')+
    theme_gray(base_size = 16)+
    theme(axis.ticks = element_line(size = 1.5, color = 'black'),
          strip.background = element_rect(fill = 'darkblue'),
          strip.text = element_text(size = 16, colour = 'white'))+
    ggtitle('Proposed Routes')+
    xlab('Longitude')+
    ylab('Latitude')
}

