# vrpoptima: Genetic Solver for a Capacitated Multiple-Depot Vehicle Routing Problem with Mileage Constraints


## Example - 3 Vehicles with 200 Visit Points
The first use case examines a set of three vehicles placed at different positions and 200 target points that need to be serviced (visited). In contrast to the standard vehicle routing problem(VRP), where all vehicles are situated at the same location(depot), thus making the problem easier, here this assumption is eased and the vehicles could be scattered at different coordinates. In the current example a capacity of 100 visits per vehicle is set. In addition, we impose a mileage constraint of 3000km. Due to the difficulty in bounding the genetic algorithm to stay within this limit a penalty is imposed on routes exceeding the specified limit to make them less attractive.

Step 1. Load the data
As an example the package comes with several optimization data sets. The optimizer expects to receive location matrices, not data frames. Therefore, we need to convert the two data frames into numeric matrices. In addition, the optimizer needs the distance matrix between the target points that the vehicles will visit in their routes.

```
# Load the package
library(vrpoptima)

# Load in memory the dummy dataset
data("patientsBG")
data("caretakersBG")

# Convert the data frames into matrices
patient_mat <- as.matrix(patientsBG)

# Compute the distance matrix
dist_mat    <- as.matrix(geodist::geodist(patient_mat, measure = 'haversine')/1000)

# Initialize the opritimizer
solution <-  VehicleRouting(visit_points = patient_mat,
                           num_agents = nrow(caretakersBG),
                           agent_points = as.matrix(caretakersBG),
                           cost_type = 2,
                           max_tour_distance = 2500,
                           max_tour_visits = 100,
                           distance_metric = 'Geodesic',
                           distance_matrix = dist_mat,
                           min_tour = 2,
                           population_size = 96,
                           num_generations = 15000, 
                           distance_truncation = TRUE, 
                           seed = 42)


# Extract the quasi-optimal routes
routes <- solution$routes
rownames(routes) <- 1:nrow(routes)
routes_list = RoutesDataPrep(routes = solution$routes, 
                             visit_points = patient_mat, 
                             agent_points = as.matrix(caretakersBG))


```

# Display the Solution
```
# Display all routes at the same time
PlotToursCombined(solution = solution, 
                  routes_list = routes_list,
                  agent_locations =  caretakersBG,
                  orientation = 'vertical')
```
![Img 1](https://gitlab.com/vikplamenov/vrpoptima/-/raw/main/im1.png)

```
# Display all the inidividual routes on a single figure block
PlotToursIndividual(solution = solution, routes_list = routes_list)
```
![Img 2](https://gitlab.com/vikplamenov/vrpoptima/-/raw/main/im2.png)
